#!/usr/bin/env python

# PowerDue Scope by Ervin Teng
# Based on code from Stimmer and others from
# http://forum.arduino.cc/index.php?topic=137635.msg1270996#msg1270996

import pyqtgraph as pg
import time, threading, sys
import serial
import serial.tools.list_ports
import numpy as np
from deviceControl import PowerDue
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import pyqtSlot,SIGNAL,SLOT
import os

import argparse

import design

NCHAN = 4
# Maximum number of task ids to show on the page.
MAX_DISP_TIDS = 40

CHANNEL_LABELS = ['Radios', 'Actuators', 'Sensors', 'Processing']

class QuadPlot(QtGui.QMainWindow, design.Ui_MainWindow):
    def __init__(self, debug):
        super(self.__class__, self).__init__()
        self.setupUi(self)  # This is defined in design.py file automatically
                            # It sets up layout and widgets that are defined
        self.plots = []
        self.plots.append(self.plot_0)
        self.plots.append(self.plot_1)
        self.plots.append(self.plot_2)
        self.plots.append(self.plot_3)

        self.initializePlots()

        self.edge_trigger = False
        self.edge_trigger_active = False
        self.setTriggerUI(self.edge_trigger)
        self.timer = QtCore.QTimer()

        # If debug mode
        self.debug = debug

        self.triggerLine = None

        self.set_0.clicked.connect(self.setButton0Clicked)
        self.startButton.clicked.connect(self.startStreaming)
        self.stopButton.clicked.connect(self.stopStreaming)
        self.openSerialButton.clicked.connect(self.openSerialPort)
        self.thread = None
        self.refreshSerialButton.clicked.connect(self.refreshSerialPorts)

        self.exportAllButton.clicked.connect(self.exportAsCSV)


        self.triggerType.currentIndexChanged[str].connect(self.changeTriggerType)

        # Numpy array for exporting
        self.temp_vals = None
        self.temp_ts = None
        self.temp_tids = None

        #self.triggerSource.currentIndexChanged[str].connect(self.plotTriggerLine)
        #self.triggerLevel.valueChanged.connect(self.plotTriggerLine)

        self.refreshSerialPorts()


    def changeTriggerType(self):
        self.stopStreaming()
        self.edge_trigger = (self.triggerType.currentText() == "Edge")
        self.setTriggerUI(self.edge_trigger)
        print self.edge_trigger

    def setTriggerUI(self, isEnabled):
        """ Enables or Disables the UI elements for edge triggering.
            - isEnabled if True, enable elements. If False, disable elements.
        """
        self.triggerLevel.setEnabled(isEnabled)
        self.triggerSource.setEnabled(isEnabled)
        self.triggerEdgeType.setEnabled(isEnabled)
        self.triggerPosition.setEnabled(isEnabled)

    def plotTriggerLine(self, clear = True):
        """ Plots a yellow horizontal line on the plot which the triggering comes from.
            clear - Set to true if we need to clear the old line
        """
        src = int(self.triggerSource.currentText())
        level = self.triggerLevel.value()
        if clear and self.triggerLine is not None and self.triggerLine.scene() is not None:
            self.triggerLine.scene().removeItem(self.triggerLine)

        # This is a hack to get rid of the null pointer messages while running,
        # but not when just setting the trigger lines.
        # TODO: find non-hack to do this.
        if clear:
            self.triggerLine = self.plots[src].addLine(y = level)
        else:
            self.plots[src].addLine(y = level)


    def startStreaming(self):
        #self.thread.startCommand()
        # Set QT refresh rate
        if self.edge_trigger:
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.updateEdgeTrig)
            self.timer.start(20)
        else:
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.update)
            self.timer.start(20)

        self.startButton.setEnabled(False)
        self.stopButton.setEnabled(True)

    def stopStreaming(self):
        #self.thread.stopCommand()
        #self.thread.exit()
        if self.timer:
            self.timer.stop()

        # Plot with task ids on stop.
        if self.temp_ts is not None and self.temp_vals is not None:
            self.plotValues(self.temp_ts, self.temp_vals, self.temp_tids)

        self.startButton.setEnabled(True)
        self.stopButton.setEnabled(False)

    def refreshSerialPorts(self):
        comportlist = []
        itportList = serial.tools.list_ports.comports()
        for port in itportList:
            if port.hwid.lower().find("c03e") > 0:
                print("Found PowerDue Instrument")
                comportlist.append(port.device)
            print port.device
        self.serialDropDown.clear()
        self.serialDropDown.addItems(comportlist)

    def openSerialPort(self):
        serialStr =  str(self.serialDropDown.currentText())
        print serialStr
        if serialStr != "":
            if self.debug:
                s = None
            else:
                s = serial.Serial(port = serialStr, timeout=10000, baudrate = 8000000)
            self.thread = PowerDue(s, debug = self.debug)
            self.thread.setDaemon(True)
            self.thread.start()

            self.openSerialButton.setStyleSheet("color: green")


    def initializePlots(self):
        i = 0
        for plt in self.plots:
            plt.setLabels(left=(CHANNEL_LABELS[i], 'V'), bottom=('Time', 's'))
            plt.setYRange(0.0, 3.3)
            i = i +1

    def setButton0Clicked(self):
        print "Clicked set Button 0"
        self.thread.setGain(0, str(self.gainSel_0.currentText()))
        self.thread.setOffset(0, str(self.offset_0.value()))
        # Calling update() will request a copy of the most recently-acquired
        # samples and plot them.

    # Update function for Edge triggering
    def updateEdgeTrig(self):
        time_scale = int(self.timeScale.currentText())
        level = self.triggerLevel.value()
        src = int(self.triggerSource.currentText())
        pos = self.triggerPosition.value()/100
        #num_samp = 100*100 # 50 ksps
        downsample = time_scale

        zoomlevel = self.zoomslider.value()
        num_samp = int(100*100*zoomlevel/10) # 50 ksps

        isrising = self.triggerEdgeType.currentText() == "Rising"

        # Grab values. If not triggered, this will return None.
        t,v, tid = self.thread.get_triggered(num_samp, downsample=downsample, pos= pos, level = level, src = src, isRising = isrising)

        # only update plots and stored values if trigged
        if t is not None:
            self.temp_ts = t
            self.temp_vals = v
            self.temp_tids = tid
            #print t, v
            self.plotValues(self.temp_ts,self.temp_vals)
            if self.triggerOnce.isChecked():
                self.stopStreaming()
                self.triggerOnce.setChecked(False)

    # Update function for Continuous triggering
    def update(self):
        time_scale = int(self.timeScale.currentText())
        zoomlevel = self.zoomslider.value()

        num_samp = int(100*100*zoomlevel/10) # 50 ksps
        downsample = time_scale
        self.temp_ts,self.temp_vals, self.temp_tids = self.thread.get(num_samp, downsample=downsample)
        #print v
        self.plotValues(self.temp_ts,self.temp_vals)
        # self.plots[i].setTitle('Sample Rate: %0.2f'%r)

        if self.triggerOnce.isChecked():
            self.stopStreaming()
            self.triggerOnce.setChecked(False)

    def plotValues(self, t, v, tids = None):
        """ Given a matrix v, which holds 4 channels of data plus task_id dimensions
        (5, X), plot it on the four plots with the color corresponding to task_id.
        if no task ids are given, don't plot them.
        Plotting task_ids is slow, so only do it on stop. """
        if tids is not None:
            # Find places where task id changes, and mark the indexes.
            tid_changes = np.where(tids[:-1] != tids[1:])[0]
            # limit plotting of lines for performance reasons
            # Downsample if there are too many.
            if tid_changes.size > MAX_DISP_TIDS:
                tid_changes = tid_changes[0:tid_changes.size:tid_changes.size/MAX_DISP_TIDS]

        for i in range(0,NCHAN):
            self.plots[i].clear()
            self.plots[i].plot(t, v[i,:])

            # Create task ID lines and text
            if tids is not None:
                # If it's all one task id, just plot it somewhere.
                if tid_changes.size == 0:
                    task_id = tids[0]
                    txt = pg.TextItem("Task ID:" + str(task_id), anchor = (1,0))
                    view_range = self.plots[i].viewRange()
                    max_y = view_range[1][1]
                    max_x = view_range[0][1]
                    txt.setPos(max_x,max_y)
                    self.plots[i].addItem(txt)
                else:
                    view_range = self.plots[i].viewRange()
                    max_y = view_range[1][1]
                    for x in np.nditer(tid_changes):
                        il = pg.InfiniteLine(pos = t[x])
                        self.plots[i].addItem(il)
                        task_id = tids[x]
                        txt = pg.TextItem(str(task_id), anchor = (1,0))
                        txt.setPos(t[x],max_y)
                        self.plots[i].addItem(txt)

    def closeEvent(self, event):
        print "Closing.."
        if self.thread:
            self.thread.exit()
            self.timer.stop()
        event.accept()

    def exportAsCSV(self, event):
        if self.temp_vals is not None:
            selfilter = "CSV Files (*.csv)";
            fileName = QtGui.QFileDialog.getSaveFileName(self, 'Export CSV File', selectedFilter=selfilter)
            if fileName:
                if self.temp_vals is not None:
                    str_fname = str(fileName)
                    header = 'time(s), ch0, ch1, ch2, ch3, task_id'
                    vals_to_export = np.vstack([self.temp_ts, self.temp_vals, self.temp_tids])
                    vals_to_export = np.transpose(vals_to_export)

                    np.savetxt(str_fname, vals_to_export, fmt="%10.6f", delimiter = ',', header = header, comments = '')
        else:
            QtGui.QMessageBox.about(self, "Error", "No data to export.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Run python oscilloscope.")
    parser.add_argument('-d', '--debug', help='Use fake data for debugging', action='store_true')
    args = parser.parse_args()
    print args.debug
    app = QtGui.QApplication(sys.argv)
    mainwindow = QuadPlot(debug = args.debug)
    mainwindow.show()

    # Start Qt event loop.
    if sys.flags.interactive == 0:
        app.exec_()
