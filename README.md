# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Scope to use PowerDue. 

### How do I get set up? ###

* Install pyqt4. This differs from platform to platform. 
* pip install -r requirements.txt

### Usage ###

* Run python powerdue_scope.py
* A scope window should open. Choose the serial port the instrumentation side of the PowerDue is on in the drop-down at the bottom left, and hit "open." The Open button should have green text. 
* Choose your triggering type and hit "Start". You can hit "Stop" to stop the plot for captures. 
* Note that there is no way to close the serial port at the moment. Hit "stop" and close the window to do that. 
* To capture plots, right-click on the plot you want and click on "export." You will be presented with a variety of options. 

### Development Notes ###

After editing the main.ui file with QTDesigner, run:

pyuic4 main.ui > design.py

To update the python file.