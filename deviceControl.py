import pyqtgraph as pg
import time, threading, sys
import serial
from time import sleep
import numpy as np
from Queue import Queue
from struct import *

NCHAN = 4
PACKET_SIZE = 1020 #bytes
HDR_SIZE = 8 #bytes, not including sync bytes
NSAMP_PER_PACKET = 16
DATA_SIZE = 2

# 4 bytes of sync
# Task ID 2 bytes
# Timestamp 4 bytes
# packet length 2 bytes
# Actual data

SYNC_BYTE = chr(0x55)
SYNC_BLOCK = 'UUUU'
SYNC_BLOCK_LENGTH = 4

STATE_SYNC = 0
STATE_READING = 1

class PowerDue(threading.Thread):
    """ Defines a thread for reading and buffering serial data.
    By default, about 5MSamples are stored in the buffer.
    Data can be retrieved from the buffer by calling get(N)"""
    def __init__(self, port, chunkSize=126, chunks=5000, debug = False):
        threading.Thread.__init__(self)
        # circular buffer for storing serial data until it is
        # fetched by the GUI

        self.bufferSize = chunks*chunkSize # Size of the buffer. Used as a utility

        # Buffer holds 4 channels + task ids.
        self.buffer = np.zeros((NCHAN + 1,self.bufferSize), dtype=np.uint16)

        self.chunks = chunks        # number of chunks to store in the buffer
        self.chunkSize = chunkSize  # size of a single chunk (items, not bytes)

        self.ptr = 0                # pointer to most (recently collected buffer index) + 1
        self.port = port            # serial port handle
        self.sps = 0.0              # holds the average sample acquisition rate
        self.exitFlag = False
        self.exitMutex = threading.Lock()
        self.dataMutex = threading.Lock()
        self.testCounter = 0
        # Queue to store commands. pulled in the run method
        self.commandQueue = Queue()

        # if dbug is on, use fake data
        self.debug = debug

    def startCommand(self):
        print "Starting..."
        #self.commandQueue.put('g')
        # Here, we send a serial command to the PowerDue to start.

    def stopCommand(self):
        print "Stopping..."
        #self.commandQueue.put('s')
        # Here, we send a serial command to the PowerDue to stop.

    def run(self):
        """ Main execution thread. Contiuously reads value from serial and puts
        it into a buffer."""

        exitMutex = self.exitMutex
        dataMutex = self.dataMutex
        buffer = self.buffer
        port = self.port
        count = 0
        sps = None
        lastUpdate = pg.ptime.time()
        tmpData = np.zeros((NCHAN + 1, self.chunkSize), dtype = np.uint16)
        print tmpData.shape
        #self.startCommand()

        while True:
            # Check if there is anything in the command queue.
            if not self.commandQueue.empty():
                command = self.commandQueue.get()
                print command
                self.port.write(command)
                self.commandQueue.task_done()
            # see whether an exit was requested
            with exitMutex:
                if self.exitFlag:
                    break

            # Fake data for debug
            if self.debug:
                readData = self.ptr*np.linspace(0, 1, self.chunkSize*NCHAN)*4096/(self.chunkSize*self.chunks)
                task_id = self.ptr % 4
                num_samples = 126
                sleep(0.000254)
            else:
                # Read full packet
                serdata = port.read(PACKET_SIZE)

                # Synchronize with packets coming from PowerDue
                if serdata[0:SYNC_BLOCK_LENGTH] != SYNC_BLOCK:
                    print serdata[0:SYNC_BLOCK_LENGTH]
                    self.synchronize()
                    serdata = port.read(PACKET_SIZE - SYNC_BLOCK_LENGTH)
                else:
                    # Remove sync bytes
                    serdata = serdata[SYNC_BLOCK_LENGTH:]

                # Grab header
                metadata = serdata[0:HDR_SIZE]

                # Unpack the metadata into Task ID (2 Bytes), Timestamp (4 Bytes), Packet Length (2 Bytes)
                md_tuples = unpack('<HIH', metadata)
                #print md_tuples

                # Get the data for each of the parts of the header
                task_id = md_tuples[0]
                timestamp = md_tuples[1]
                packet_length = md_tuples[2]

                packet_length = packet_length - packet_length%8

                # Only recognize the packet_length number of values
                actualdata = serdata[HDR_SIZE:packet_length+HDR_SIZE]

                # Compute the number of samples in packet_length
                num_samples = packet_length /4/2 if packet_length < PACKET_SIZE-HDR_SIZE else 126

                # convert data to 16bit int numpy array
                readData = np.fromstring(actualdata, dtype=np.uint16)

                #print readData.shape

            #readData = np.arange(0, self.chunkSize*NCHAN)
            for _chan in range(0, NCHAN):
                # Subsample. Start:stop:step
                #print self.chunkSize - NCHAN + _chan + 1
                #print readData[_chan:self.chunkSize - NCHAN + _chan + 1:4]
                tmpData[_chan, 0:num_samples] = ((readData[_chan:readData.size:4]).copy() & 0x0FFF)


            # Write Task IDs to buffer as well
            tmpData[NCHAN,0:num_samples] = task_id * np.ones((1, num_samples))
            # write the new chunk into the circular buffer
            # and update the buffer pointer
            with dataMutex:
                # Check for the end of a buffer. If it's the end, loop back.
                if self.ptr + num_samples > self.bufferSize:
                    # Leftover samples
                    delta = self.ptr + num_samples - self.bufferSize
                    buffer[:,self.ptr:self.bufferSize] = tmpData[:,0:num_samples - delta]
                    buffer[:,0:delta] = tmpData[:,num_samples-delta: num_samples]
                else:
                    buffer[:,self.ptr:self.ptr+num_samples] = tmpData[:,0:num_samples]
                self.ptr = (self.ptr + num_samples) % buffer.shape[1]

        port.close()

    def synchronize(self):
        """ Synchronize serial reads with the packts. Looks for SYNC_BYTE repeated
        four times as the packet header.
        """
        synced = False
        num_count = 0
        while not synced:
            syncb = self.port.read(1)
            if syncb == SYNC_BYTE:
                num_count = num_count + 1
            else:
                num_count = 0
            if num_count == 4:
                synced = True
        print "synced"

    def get_triggered(self, num, downsample, pos, level, src, isRising):
        """ Return a tuple (time_values, voltage_values, rate), where the values
        of channel src cross level at position pos (as a fraction of the total length)
          - voltage_values will contain the *num* most recently-collected samples
            as a 32bit float array.
          - time_values assumes samples are collected at 50 ksps
          - rate is the running average sample rate.
          - pos is the position of the trigger crossing.
          - level is the triggering level.
          - src is the channel on which to trigger
          - isRising if triggering on rising edge, trigger on falling edge otherwise.
        If *downsample* is > 1, then the number of values returned will be
        reduced by averaging that number of consecutive samples together. In
        this case, the voltage array will be returned as 32bit float.
        """
        # Get the entire buffer, aligned
        t, searchbuf, tids = self.get(self.chunks*self.chunkSize, downsample = downsample)
        # Search for first value greater than level
        #print searchbuf

        # downsample if need be
        num = num/downsample
        if isRising:
            idx = np.argmax(searchbuf[src] > level)
        else:
            idx = np.argmax(searchbuf[src] < level)
        trig_pos_idx = int(num*pos)
        #print idx
        if idx > trig_pos_idx and idx - trig_pos_idx + num < self.chunks*self.chunkSize:
            if downsample > 1:
                tstamps = np.linspace(0, (num-1)*2e-5*downsample, num)
            else:
                tstamps = np.linspace(0, (num-1)*2e-5, num)
            return tstamps, searchbuf[0:NCHAN,idx-trig_pos_idx:idx - trig_pos_idx + num ], tids[idx-trig_pos_idx:idx - trig_pos_idx + num]
        else:
            return None, None, None

    def get(self, num, downsample=1):
        """ Return a tuple (time_values, voltage_values, rate)
          - voltage_values will contain the *num* most recently-collected samples
            as a 32bit float array.
          - time_values assumes samples are collected at 50 ksps
          - rate is the running average sample rate.
        If *downsample* is > 1, then the number of values returned will be
        reduced by averaging that number of consecutive samples together. In
        this case, the voltage array will be returned as 32bit float.
        """
        with self.dataMutex:  # lock the buffer and copy the requested data out
            ptr = self.ptr
            if ptr-num < 0:
                data = np.empty((NCHAN + 1,num), dtype=np.uint16)
                data[:,:num-ptr] = self.buffer[:,ptr-num:]
                data[:,num-ptr:] = self.buffer[:,:ptr]
            else:
                data = self.buffer[:,self.ptr-num:self.ptr].copy()

        # Convert array to float and rescale to voltage.
        # Assume 3.3V / 12bits

        vals = data[0:NCHAN,:].astype(np.float32) * (3.3 / 2**12)
        tids = data[NCHAN,:]

        if downsample > 1:  # if downsampling is requested, average N samples together
            ds_data= vals.reshape((NCHAN,num/downsample,downsample)).mean(axis=2)
            # Don't average the taskids
            tids = tids[0::downsample]
            num_ds = ds_data.shape[1]
            return np.linspace(0, (num_ds-1)*2e-5*downsample, num_ds), ds_data, tids
        else:
            return np.linspace(0, (num-1)*2e-5, num), vals, tids

    def exit(self):
        """ Instruct the serial thread to exit."""
        self.stopCommand()
        with self.exitMutex:
            self.exitFlag = True

    def setGain(self, channel, gainSel):
        """Set the gain on a particular channel. The values of gainSel are 25, 50, 100, 200.
        channel should be either 0 1 2  or 3.
        """
        command = "channel" + str(channel) + "gainsel" + str(gainSel)
        self.commandQueue.put(command)

    def setOffset(self, channel, offset):
        command = "channel" + str(channel) + "offset" + str(offset)
        self.commandQueue.put(command)
